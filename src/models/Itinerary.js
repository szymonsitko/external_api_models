const Sequelize = require('sequelize');

module.exports = (sequelize) => {
  const Itinerary = sequelize.define('Itinerary', {
    operatorId: {
      type: Sequelize.BIGINT(20),
      allowNull: false,
      field: 'operator_id'
    },
    referenceCode: {
      type: Sequelize.STRING(64),
      allowNull: false,
      field: 'reference_code'
    },
    clientReference: {
      type: Sequelize.STRING(128),
      allowNull: false,
      field: 'client_reference'
    },
    departureDate: {
      type: Sequelize.DATE,
      allowNull: false,
      field: 'departure_date'
    },
    returnDate: {
      type: Sequelize.DATE,
      allowNull: false,
      field: 'return_date'
    },
    leadTraveller: {
      type: Sequelize.BIGINT(20),
      allowNull: false
    },
    field1: {
      type: Sequelize.STRING(128),
      allowNull: false
    },
    field2: {
      type: Sequelize.STRING(128),
      allowNull: false
    },
    field3: {
      type: Sequelize.STRING(128),
      allowNull: false
    },
    field4: {
      type: Sequelize.STRING(128),
      allowNull: false
    },
    source: {
      type: Sequelize.STRING(16)
    },
    copyOfId: {
      type: Sequelize.BIGINT(20),
      field: 'copy_of_id'
    },
    backgroundFileId: {
      type: Sequelize.BIGINT(20),
      field: 'background_file_id'
    },
    isActive: {
      type: Sequelize.SMALLINT(6),
      allowNull: false,
      field: 'is_active'
    },
    logoFileId: {
      type: Sequelize.BIGINT(20),
      field: 'logo_file_id'
    },
    parentId: {
      type: Sequelize.BIGINT(20),
      field: 'parent_id'
    },
    deactivatedTs: {
      type: Sequelize.DATE,
      field: 'deactivated_ts'
    },
    deactivateBy: {
      type: Sequelize.BIGINT(20),
      field: 'deactivated_by'
    },
    createdTs: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.NOW,
      field: 'created_ts',
      allowNull: false
    },
    updatedTs: {
      type: Sequelize.DATE,
      field: 'updated_ts'
    }
  }, { tableName: 'itinerary', timestamps: false });
  return Itinerary;
};
